#!/usr/bin/env janet
(import ./jddclient/detect :as detect)
(import spork/argparse)
(import sh)

(def- help-text
  ``
  '--ip interface,<interface-name>' obtains IP address from a network interface
  '--ip web' fetches IP address from the internet

  '--protocol afraid,update-token' updates a domain name on freedns.afraid.org
  ``)

(defn- send-mail
  [email subject content]
  (sh/$ mail -s ,subject ,email < ,content))

(defn- update-on-interval
  [ip-addr num-errors data]
  (let [{:ip-addr-func ipf :update-func updatef
         :email email :interval intv :verbose verbose} data
        new-ip-addr (ipf)]
    (if (= ip-addr new-ip-addr)
      (do
        (os/sleep intv)
        (update-on-interval ip-addr 0 data))
      (do
        (let [{:success success :response resp}
              (updatef verbose new-ip-addr)]
          (when (not success)
            (if-not verbose (print "(Error) " resp))
            (send-mail email "jddclient failure" resp)
            (when (<= 5 (inc num-errors))
              (send-mail email "jddclient failure"
                         "Quitting after failing 5 times consecutively.")
              (print "Quitting after failing 5 times consecutively.")
              (os/exit 1)))
          (os/sleep intv)
          (if success
            (update-on-interval new-ip-addr 0 data)
            (update-on-interval ip-addr (inc num-errors) data)))))))

(defn main [& args]
  (when-let [{"ip" ip "protocol" proto "email" email "interval" interval-s
              "verbose" verbose}
             (argparse/argparse help-text
                                "email"
                                {:kind :option
                                 :help "Report errrors to this email address"
                                 :required true}
                                "ip" {:kind :option
                                      :help "IP address detection method"
                                      :required true}
                                "protocol" {:kind :option
                                            :help "dyndns protocol"
                                            :required true}
                                "interval"
                                {:kind :option
                                 :help "number of seconds between updates"
                                 :default "300"}
                                "verbose" {:kind :flag
                                           :help "Verbose output"
                                           :default false})]
    (let [ip-addr-func (detect/ip-addr-func ip)
          update-func (detect/update-func proto)
          interval (scan-number interval-s)]
      (when (nil? ip-addr-func)
        (print (string ip " is not a supported ip address detection method."))
        (os/exit 1))
      (when (nil? update-func)
        (print (string proto " is not a correct protocol."))
        (os/exit 1))
      (when (nil? interval)
        (print (string interval-s " is not a correct interval number."))
        (os/exit 1))
      (update-on-interval nil 0
                          {:ip-addr-func ip-addr-func :update-func update-func
                           :email email :interval interval :verbose verbose}))))
