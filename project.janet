(declare-project
  :name "jddclient"
  :description "Dynamic DNS client"
  :dependencies ["https://github.com/janet-lang/spork.git"
                 "https://github.com/andrewchambers/janet-sh.git"
                 "https://github.com/joy-framework/http.git"])

(declare-executable
  :name "jddclient"
  :entry "main.janet"
  :install true)
