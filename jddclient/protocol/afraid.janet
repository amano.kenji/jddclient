(import http)

(defn set-ip-address-with-token
  ```
  Try to set IP address with update token.
  ```
  [update-token &opt verbose ip-addr]
  (let [url (string "https://sync.afraid.org/u/" update-token "/"
                    (if ip-addr (string "?ip=" ip-addr)))
        output (string/trim (get (http/get url) :body))]
    (if verbose
      (print output))
    {:success (or (string/has-prefix? "No IP change detected for" output)
                  (string/has-prefix? "Updated " output))
     :response output}))
