(def- ip-peg (peg/compile '{:af (+ "inet6" "inet")
                            :main (* (some (if-not (* " " :af " ") 1))
                                     " " :af " "
                                     '(some (if-not (set " /\n") 1))
                                     (some (if-not " scope " 1))
                                     " scope global ")}))

(defn get-ip-address
  [intf]
  (let [output (-> (os/spawn ["ip" "addr" "show" "dev" intf]
                             :p {:out :pipe})
                   (get :out)
                   (ev/read :all))]
    (get (peg/match ip-peg output) 0)))
