(import http)

(def- ip-peg (peg/compile ~(* (some (if-not "<body>" 1))
                              "<body>Current IP Address: "
                              '(some (if-not "<" 1)))))

(defn get-ip-address
  []
  (let [output (get (http/get "http://checkip.dyndns.org/") :body)]
    (get (peg/match ip-peg output) 0)))
