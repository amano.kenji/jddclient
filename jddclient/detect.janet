(import ./ip/web :as web)
(import ./ip/interface :as interface)
(import ./protocol/afraid :as afraid)

(def- ip-addr-detection-funcs
  [(fn [ip]
     (when-let [intf (-> (peg/match
                           '(* "interface," '(some (if-not "," 1)))
                           ip)
                         (get 0))] 
       (fn [] (interface/get-ip-address intf))))
   (fn [ip]
     (when (= ip "web")
       web/get-ip-address))])

(def- update-protocol-funcs
  [(fn [proto]
     (when-let [token (get (peg/match '(* "afraid," '(some (if-not "," 1)))
                                      proto)
                           0)]
       (fn [verbose ip-addr]
         (afraid/set-ip-address-with-token token verbose ip-addr))))])

(defn ip-addr-func
  [ip]
  (some |($ ip) ip-addr-detection-funcs))

(defn update-func
  [protocol]
  (some |($ protocol) update-protocol-funcs))
